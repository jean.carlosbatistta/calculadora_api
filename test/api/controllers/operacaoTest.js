var should = require('should');
var request = require('supertest');
var server = require('../../../app');

describe('controllers', function() {

  describe('operacaoController', function() {

    describe('GET /multiplicacoes', function() {

      it('Operação de Multiplicação', function(done) {

        request(server)
          .get(`/multiplicacoes`)
          .query({ numero1: 2, numero2: 3 })
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            should.not.exist(err);
            res.body.should.eql(6);
            done();
          });
      });
    });

    describe('GET /multiplicacoes', function() {

        it('Operação de Potenciação', function(done) {
    
            request(server)
              .get('/potenciacao')
              .query({ numero: 2, potencia: 5})
              .set('Accept', 'application/json')
              .expect('Content-Type', /json/)
              .expect(200)
              .end(function(err, res) {
                should.not.exist(err);
    
                res.body.should.eql(32);
    
                done();
              });
          });
      });

      describe('GET /raizcubica', function() {
        
          it('Operação Raiz Cubica', function(done) {
      
            request(server)
              .get('/raizcubica')
              .query({ numero: 8})
              .set('Accept', 'application/json')
              .expect('Content-Type', /json/)
              .expect(200)
              .end(function(err, res) {
                should.not.exist(err);
    
                res.body.should.eql(2);
    
                done();
              });
          });    
          
          it('Operação Raiz Cubica número inválido', function(done) {
            
              request(server)
              .get('/raizcubica')
              .query({ numero: "oito"})
              .set('Accept', 'application/json')
              .expect('Content-Type', /json/)
              .expect(400)
              .end(function(err, res) {
                should.not.exist(err);
                res.status.should.eql(400);
          
                done();
                
              });
          });          
      });      
    });   
});
