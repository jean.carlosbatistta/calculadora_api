'use strict';

/** Multiplicação */
exports.multiplicar = async function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    try {
        let numero1 = req.swagger.params.numero1.value;
        let numero2 = req.swagger.params.numero2.value;
        let result = numero1 * numero2;
        res.end(result);   
    } catch (error) {
        res.status(500).end("internal server error"); 
    }
}

/** Raiz Cubica */
exports.calcularRaizCubica = async function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    try {
        let numero = req.swagger.params.numero.value;
        let result = Math.pow(numero, 1/3);
        res.end(result);   
    } catch (error) {
        res.status(500).end("internal server error"); 
    }
}

/** Potencição */
exports.calcularPotenciacao = async function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    try {
        let numero = req.swagger.params.numero.value;
        let potencia = req.swagger.params.potencia.value;
        let result = Math.pow(numero, potencia);
        res.end(result);   
    } catch (error) {
        res.status(500).end("internal server error"); 
    }
}