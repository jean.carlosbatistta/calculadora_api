'use strict';
const service = require('../service/operacaoService');

/** Multiplicação */
module.exports.multiplicar = (req, res) => {
    service.multiplicar(req, res);
}

/** Multiplicação */
module.exports.calcularRaizCubica = (req, res) => {
  service.calcularRaizCubica(req, res);
}


/** Potenciação */
module.exports.calcularPotenciacao = (req, res) => {
  service.calcularPotenciacao(req, res);
}